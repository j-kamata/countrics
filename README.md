# Countrics

2022/03/01

## You will need to install it in your runtime environment

- volta
- pnpm
- sqlite3

## Extensions used in this project (VS Code)

- ESLint
- Prettier
- SQLite

## How to run it

```bash
$ git clone countrics
$ cd countrics
$ pnpm install
$ pnpm start --filter crawler
```
