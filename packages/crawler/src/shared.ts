import jquery from 'jquery'
import { JSDOM } from 'jsdom'
import { UA, Result } from '@/crawler.config'
import { $dayjs } from '@/plugins/dayjs'

let enrichment: (address: string) => Promise<{ 住所: { 都道府県: string } }>
let zip: typeof import('lodash-es/zip').default
let zipObject: typeof import('lodash-es/zipObject').default
let inRange: typeof import('lodash-es/inRange').default
let chalk: typeof import('chalk').default

void (async () => {
  // Change to dynamic import() available in CommonJS modules
  chalk = (await import('chalk')).default

  const lodash = (await import('lodash-es')).default

  zip = lodash.zip
  zipObject = lodash.zipObject
  inRange = lodash.inRange

  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  enrichment = (await import('imi-enrichment-address')).default as (
    address: string
  ) => Promise<{ 住所: { 都道府県: string } }>
})()

export const newSession = async (browser: import('playwright').Browser, userAgent: Partial<UA>, timeout = 20000) => {
  const context = await browser.newContext({ userAgent })
  const page = await context.newPage()
  page.setDefaultTimeout(timeout)
  return { context, page }
}

export const html2jquery = (html: string) => {
  const dom = new JSDOM(html)
  const $ = jquery(dom.window) as unknown as JQueryStatic
  return $
}

export const convert = (parsed: string[][]) => {
  const results = zip(...parsed).map(entry => zipObject(['name', 'age', 'residence'], entry)) as Result[]
  return results
}

export const age2gen = (age: string) => {
  const year = Number($dayjs().format('YYYY'))
  const birth =
    year -
    (Number(age.match(/^\d{2}/)) + (/前半/.test(age) ? 2 : 0) + (/半ば/.test(age) ? 5 : 0) + (/後半/.test(age) ? 8 : 0))
  const gen = inRange(birth, 1928, 1945)
    ? 'T' // Traditionalist
    : inRange(birth, 1945 - 1, 1964 + 1)
    ? 'BB' // Baby boomers
    : inRange(birth, 1965 - 1, 1980)
    ? 'X' //  Generation X
    : inRange(birth, 1980 - 1, 1995)
    ? 'Y' //  Generation Y
    : inRange(birth, 1995 - 1, year)
    ? 'Z' //  Generation Z
    : ''
  return gen
}

export const residence2pref = async (residence: string) => {
  let pref = (await enrichment(residence)).住所.都道府県
  pref = /港区/.test(residence) ? '東京都' : pref // There is more than one port district, and the decision is not functioning properly (WKWK)
  return pref ?? residence // abroad
}

export const residence2area = (residence: string, pref: string) => {
  const area = residence
    .replace(pref, '')
    .replace(/\(.*\)/, '')
    .replace(/道央|道北|道南|道東/, '')
  return area === '福岡' && pref === '福井県' ? '' : area // Bug in the service side (HPM)
}

export const notifyMessage = (message: string) => {
  console.log(chalk.cyan(message))
}

export const notifyError = (error: unknown) => {
  if (error instanceof Error) {
    // Don't display obvious error messages
    if (!['already exists', 'Navigation failed because page was closed!'].includes(error.message)) return
    console.log(chalk.red('An error has occurred'))
    console.error(error.message)
  }
}
