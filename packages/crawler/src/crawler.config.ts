import { HPM } from '@/modules/HPM'
import { PCM } from '@/modules/PCM'
import { WKPL } from '@/modules/WKPL'
import { WKWK } from '@/modules/WKWK'
import { YYC } from '@/modules/YYC'

export type Crawler = {
  crawl: (browser: import('playwright').Browser, sex: SEX, isDebug: boolean) => Promise<Result[]>
  gid: Partial<GID>
  sex: Partial<SEX>
}

export type Result = {
  age: string
  name: string
  residence: string
}

export enum SEX {
  _,
  MALE,
  FEMALE
}

export const SEX_LABEL = {
  [SEX._]: '',
  [SEX.MALE]: '男',
  [SEX.FEMALE]: '女'
}

export enum GID {
  _,
  WKWK_MALE,
  WKWK_FEMALE,
  YYC_MALE,
  YYC_FEMALE,
  PCM_MALE,
  PCM_FEMALE,
  HPM_MALE,
  HPM_FEMALE,
  WKPL_MALE,
  WKPL_FEMALE
}

export const SERVICE = {
  [GID._]: '',
  [GID.WKWK_MALE]: 'ワクワクメール',
  [GID.WKWK_FEMALE]: 'ワクワクメール',
  [GID.YYC_MALE]: 'YYC',
  [GID.YYC_FEMALE]: 'YYC',
  [GID.PCM_MALE]: 'PCMAX',
  [GID.PCM_FEMALE]: 'PCMAX',
  [GID.HPM_MALE]: 'ハッピーメール',
  [GID.HPM_FEMALE]: 'ハッピーメール',
  [GID.WKPL_MALE]: 'ワクプラ',
  [GID.WKPL_FEMALE]: 'ワクプラ'
}

// For Google Spread Sheets
export const CELL = {
  [GID._]: '',
  [GID.WKWK_MALE]: 'B',
  [GID.WKWK_FEMALE]: 'C',
  [GID.YYC_MALE]: 'E',
  [GID.YYC_FEMALE]: 'F',
  [GID.PCM_MALE]: 'H',
  [GID.PCM_FEMALE]: 'I',
  [GID.HPM_MALE]: 'K',
  [GID.HPM_FEMALE]: 'L',
  [GID.WKPL_MALE]: 'N',
  [GID.WKPL_FEMALE]: 'O'
}

export enum MAX {
  WKWK = 20,
  YYC = 10,
  PCM = 27,
  HPM = 50, // Default is 10, however, the default value will quickly overflow
  WKPL = 20
}

export enum UA {
  DESKTOP = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
  MOBILE = 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.3 Mobile/15E148 Safari/604.1'
}

export const crawlers: Crawler[] = [
  { crawl: WKWK, gid: GID.WKWK_MALE, sex: SEX.MALE },
  { crawl: YYC, gid: GID.YYC_MALE, sex: SEX.MALE },
  { crawl: PCM, gid: GID.PCM_MALE, sex: SEX.MALE },
  { crawl: HPM, gid: GID.HPM_MALE, sex: SEX.MALE },
  { crawl: WKPL, gid: GID.WKPL_MALE, sex: SEX.MALE },
  { crawl: WKWK, gid: GID.WKWK_FEMALE, sex: SEX.FEMALE },
  { crawl: YYC, gid: GID.YYC_FEMALE, sex: SEX.FEMALE },
  { crawl: PCM, gid: GID.PCM_FEMALE, sex: SEX.FEMALE },
  { crawl: HPM, gid: GID.HPM_FEMALE, sex: SEX.FEMALE },
  { crawl: WKPL, gid: GID.WKPL_FEMALE, sex: SEX.FEMALE }
]
