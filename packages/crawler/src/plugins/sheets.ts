import { google, sheets_v4 } from 'googleapis'

export const authentication = async () => {
  const auth = new google.auth.GoogleAuth({
    keyFile: 'credentials.json',
    scopes: 'https://www.googleapis.com/auth/spreadsheets'
  })
  const client = await auth.getClient()
  const sheets = google.sheets({ auth: client, version: 'v4' })
  return { sheets }
}

export const clearRows = async (sheets: sheets_v4.Sheets, range: string) => {
  await sheets.spreadsheets.values.clear({
    range: range,
    spreadsheetId: '1Lw5rH3MI6QKl5o41NUviC9OYjzgEqEtBf2n7w9us18U'
  } as sheets_v4.Params$Resource$Spreadsheets$Values$Clear)
}

export const addRows = async (
  sheets: sheets_v4.Sheets,
  range: string,
  values: (string | number)[][],
  insertDataOption: 'INSERT_ROWS' | 'OVERWRITE'
) => {
  await sheets.spreadsheets.values.append({
    insertDataOption,
    range: range,
    resource: { values },
    spreadsheetId: '1Lw5rH3MI6QKl5o41NUviC9OYjzgEqEtBf2n7w9us18U',
    valueInputOption: 'USER_ENTERED'
  } as sheets_v4.Params$Resource$Spreadsheets$Values$Append)
}
