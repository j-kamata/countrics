import { Knex, knex } from 'knex'
import { GID } from '@/crawler.config'

export interface Data {
  age: string
  area: string
  code: string
  date: string
  day: string
  gen: string
  gid: Partial<GID>
  is_holiday: string
  name: string
  pref: string
  service: string
  sex: string
  time: string
}

export interface Record extends Data {
  id: number
}

export interface Summary {
  code: string
  count: number
  created_at: string
  gid: number
  id: number
  service: string
  sex: string
  updated_at: string
  updates: number
}

export interface History {
  code: string
  count: number
  count_time: string
  created_at: string
  gid: number
  id: number
  service: string
  sex: string
}

const config: Knex.Config = {
  client: 'sqlite3',
  connection: { filename: '../../db/production.sqlite3' },
  useNullAsDefault: true
}

export const database = knex(config)

export const createUserTable = async () => {
  await database.schema.createTable('user', table => {
    table.increments('id')
    table.integer('gid')
    table.string('code')
    table.string('service')
    table.string('sex')
    table.string('name')
    table.string('age')
    table.string('gen')
    table.string('pref')
    table.string('area')
    table.date('date')
    table.string('day')
    table.string('is_holiday')
    table.time('time')
  })
}

export const createSummaryTable = async () => {
  await database.schema.createTable('summary', table => {
    table.increments('id')
    table.integer('gid')
    table.string('code')
    table.string('service')
    table.string('sex')
    table.integer('count')
    table.date('created_at')
    table.date('updated_at')
    table.integer('updates')
  })
}

export const createHistoryTable = async () => {
  await database.schema.createTable('history', table => {
    table.increments('id')
    table.integer('gid')
    table.string('code')
    table.string('service')
    table.string('sex')
    table.integer('count')
    table.string('count_time')
    table.date('created_at')
  })
}
