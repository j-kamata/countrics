import { SEX, UA } from '@/crawler.config'
import { newSession, html2jquery, convert } from '@/shared'

export const PCM = async (browser: import('playwright').Browser, sex: SEX, isDebug: boolean) => {
  const fetch = async () => {
    // set::
    const { page, context } = await newSession(browser, UA.MOBILE)
    // login::
    await page.goto('https://pcmax.jp/pcm/file.php?f=login_form')
    await page.type('input[name="login_id"]', sex === SEX.MALE ? '17218479' : SEX.FEMALE ? '17217970' : '')
    await page.type('input[name="login_pw"]', sex === SEX.MALE ? '19800816' : SEX.FEMALE ? '19780513' : '')
    await page.click('button[name="login"]')
    // render:: Cannot be accessed from overseas IPs due to WAF settings
    await page.goto('https://pcmax.jp/mobile/profile_reference.php')
    if (!(await page.isChecked('#pref_all'))) await page.click('.purbox') // [important] All Prefectures
    await page.selectOption('#so1', '2') // [important] Registration Order
    await page.click('#search1')
    await page.waitForLoadState('domcontentloaded')
    const html = await page.content()
    // logout::
    await page.goto('https://pcmax.jp/?logout=1')
    // close::
    await context.close()
    // return::
    return html
  }

  const parse = (key: string, selector: string) => {
    const results = $.map($(selector), htmlElement => {
      const text = $(htmlElement as HTMLElement).text()
      // Since the given character set is unknown and regular expression substitution can be more confusing,
      // convert to Unicode before use
      const unicodes = Array.from({ length: text.length }).map((_, index) => text.charCodeAt(index))
      if (isDebug) console.log({ text, unicodes })
      const result =
        key === 'name'
          ? text.split(text[unicodes.findIndex(unicode => unicode === 46)])[0].trim()
          : key === 'age'
          ? text
          : key === 'residence'
          ? text
              .replace(/^登録地域/, '')
              .split(text[unicodes.findIndex(unicode => unicode === 32)])[0]
              .trim()
          : ''
      return result
    })
    return results
  }

  const html = await fetch()
  const $ = html2jquery(html)
  const selectors = new Map([
    ['name', '.user_info .name'],
    ['age', '.user_info .age'],
    ['residence', sex === SEX.MALE ? '.user_info + .conf' : SEX.FEMALE ? '.user_info .pref' : '']
  ])
  const parsed = Array.from(selectors).map(([key, selector]) => parse(key, selector))
  const results = convert(parsed)
  return results
}
