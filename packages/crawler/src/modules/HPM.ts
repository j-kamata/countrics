import { SEX, UA } from '@/crawler.config'
import { newSession, html2jquery, convert } from '@/shared'

export const HPM = async (browser: import('playwright').Browser, sex: SEX, isDebug: boolean) => {
  const fetch = async () => {
    // set::
    const { page, context } = await newSession(browser, UA.MOBILE, 30000)
    // login::
    await page.goto('https://happymail.co.jp/login/')
    await page.type('#TelNo', sex === SEX.MALE ? '08090900570' : SEX.FEMALE ? '09063517571' : '')
    await page.type('input[name="Pass"]', sex === SEX.MALE ? '1980' : SEX.FEMALE ? '0513' : '')
    if (await page.isChecked('#loginbtn_chk')) await page.click('label[for="loginbtn_chk"]')
    await page.click('#login_btn')
    // render::
    await page.goto('https://happymail.co.jp/sp/app/html/profile_list.php')
    await page.selectOption('#order_select', '1')
    await page.selectOption('#kind_select', '0')
    await page.waitForResponse('https://happymail.co.jp/sp/app/api/html/list_profile.php')
    // 50 record
    for (let i = 0; i < 5 - 1; i++) {
      await page.click('#load_list_profile')
      await page.waitForResponse('https://happymail.co.jp/sp/app/api/html/list_profile.php')
    }
    const html = await page.content()
    // logout::
    await page.goto('https://happymail.co.jp/sp/app/html/logout.php?result=on')
    // close::
    await context.close()
    // return::
    return html
  }

  const parse = (key: string, selector: string) => {
    const results = $.map($(selector), htmlElement => {
      const text = $(htmlElement as HTMLElement).text()
      // Since the given character set is unknown and regular expression substitution can be more confusing,
      // convert to Unicode before use
      const unicodes = Array.from({ length: text.length }).map((_, index) => text.charCodeAt(index))
      if (isDebug) console.log({ text, unicodes })
      const result =
        key === 'name'
          ? text.split(text[unicodes.findIndex(unicode => unicode === 160)])[1].trim()
          : key === 'age'
          ? text.split(text[unicodes.findIndex(unicode => unicode === 160)])[0]
          : key === 'residence'
          ? text.split(text[unicodes.findIndex(unicode => unicode === 160)])[1]
          : ''
      return result
    })
    return results
  }

  const html = await fetch()
  const $ = html2jquery(html)
  const selectors = new Map([
    ['name', '.ds_post_body_name_small'],
    ['age', '.ds_post_body_age_small'],
    ['residence', '.ds_post_body_age_small']
  ])
  const parsed = Array.from(selectors).map(([key, selector]) => parse(key, selector))
  const results = convert(parsed)
  return results
}
