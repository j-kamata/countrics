import { SEX, UA } from '@/crawler.config'
import { newSession, html2jquery, convert } from '@/shared'

export const YYC = async (browser: import('playwright').Browser, sex: SEX, isDebug: boolean) => {
  const fetch = async () => {
    // set::
    const { page, context } = await newSession(browser, UA.DESKTOP)
    // login::
    await page.goto('https://www.yyc.co.jp/login/')
    await page.type('input[name="account"]', sex === SEX.MALE ? '18344340' : SEX.FEMALE ? '18344119' : '')
    await page.type('input[name="password"]', sex === SEX.MALE ? '19800816' : SEX.FEMALE ? '19780513' : '')
    if (await page.isChecked('#auto_login')) await page.click('label[for="auto_login"]')
    await page.click('input[value="ログイン"]')
    // render::
    await page.goto('https://www.yyc.co.jp/profile/search/result')
    const html = await page.content()
    // logout::
    await page.goto('https://www.yyc.co.jp/logout/')
    // close::
    await context.close()
    // return::
    return html
  }

  const parse = (key: string, selector: string) => {
    const results = $.map($(selector), htmlElement => {
      const text = $(htmlElement as HTMLElement).text()
      // Since the given character set is unknown and regular expression substitution can be more confusing,
      // convert to Unicode before use
      const unicodes = Array.from({ length: text.length }).map((_, index) => text.charCodeAt(index))
      if (isDebug) console.log({ text, unicodes })
      const result =
        key === 'name'
          ? text
          : key === 'age'
          ? text.split(text[unicodes.findIndex(unicode => unicode === 160)])[
              unicodes.filter(unicode => unicode === 160).length
            ]
          : key === 'residence'
          ? `${text.split(text[unicodes.findIndex(unicode => unicode === 160)])[0]}${
              text.split(text[unicodes.findIndex(unicode => unicode === 160)])[1]
            }`
          : ''
      return result
    })
    return results
  }

  const html = await fetch()
  const $ = html2jquery(html)
  const selectors = new Map([
    ['name', 'td.name h3'],
    ['age', 'td.name h3 + ul > li:first-child'],
    ['residence', 'td.name h3 + ul > li:first-child']
  ])
  const parsed = Array.from(selectors).map(([key, selector]) => parse(key, selector))
  const results = convert(parsed)
  return results
}
