import { SEX, UA } from '@/crawler.config'
import { newSession, html2jquery, convert } from '@/shared'

export const WKPL = async (browser: import('playwright').Browser, sex: SEX, isDebug: boolean) => {
  const fetch = async () => {
    // set::
    const { page, context } = await newSession(browser, UA.MOBILE)
    // login::
    await page.goto('https://wakupl.com/login/')
    await page.type('#email', sex === SEX.MALE ? '08090900570' : SEX.FEMALE ? '09063517571' : '')
    await page.type('#password', sex === SEX.MALE ? '1980' : SEX.FEMALE ? '0513' : '')
    if (await page.isChecked('#auto1')) await page.click('label[for="auto1"]')
    await page.click('#login_btn')
    // render::
    await page.goto('https://wakupl.com/m/search/')
    const value = await page.content()
    if (!/全国/.test(value)) {
      console.error('Not all prefectures!')
    }
    await page.selectOption('select[name="sort"]', 'register') // [important] Registration Order
    await page.click('input[name="list"]')
    await page.waitForLoadState('domcontentloaded')
    const html = await page.content()
    // logout:: There is no logout mechanism in WKPL
    // close::
    await context.close()
    // return::
    return html
  }

  const parse = (key: string, selector: string) => {
    const results = $.map($(selector), htmlElement => {
      const text = $(htmlElement as HTMLElement).text()
      // Since the given character set is unknown and regular expression substitution can be more confusing,
      // convert to Unicode before use
      const unicodes = Array.from({ length: text.length }).map((_, index) => text.charCodeAt(index))
      if (isDebug) console.log({ text, unicodes })
      const result =
        key === 'name'
          ? text
          : key === 'age'
          ? text.split(text[unicodes.findIndex(unicode => unicode === 10)])[0]
          : key === 'residence'
          ? text.split(text[unicodes.findIndex(unicode => unicode === 160)])[0]
          : ''
      return result
    })
    return results
  }

  const html = await fetch()
  const $ = html2jquery(html)
  const selectors = new Map([
    ['name', '.area_contents p a'],
    ['age', '.area_contents tr td:nth-child(2)'],
    ['residence', '.area_contents tr td:nth-child(2) span']
  ])
  const parsed = Array.from(selectors).map(([key, selector]) => parse(key, selector))
  const results = convert(parsed)
  return results
}
