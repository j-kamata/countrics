import { between } from '@holiday-jp/holiday_jp'
import { chromium } from 'playwright'
import prettyMilliseconds from 'pretty-ms'
import { GID, SEX_LABEL, SERVICE, crawlers, Crawler, MAX, CELL } from '@/crawler.config'
import { $dayjs } from '@/plugins/dayjs'
import {
  database,
  createUserTable,
  createSummaryTable,
  createHistoryTable,
  Data,
  Record,
  Summary,
  History
} from '@/plugins/knex'
import { authentication, addRows, clearRows } from '@/plugins/sheets'
import { age2gen, residence2pref, notifyError, residence2area, notifyMessage } from '@/shared'

const run = async (
  crawler: Crawler,
  browser: import('playwright').Browser,
  sheets: import('googleapis').sheets_v4.Sheets,
  isDebug = false
) => {
  const count_start = Date.now()

  const { gid, sex } = crawler

  const name = Object.entries(GID)[gid][1] as keyof typeof GID

  notifyMessage(`crawling... >>> ${name}`)

  // crawler crawl::
  const results = await crawler.crawl(browser, sex, isDebug).catch(notifyError)

  if (isDebug) console.log({ results })

  // set values::
  const code = name.split('_')[0] as keyof typeof MAX
  const date = $dayjs().format('YYYY-MM-DD')
  const day = $dayjs(date).format('dd')
  const holidays = between($dayjs(date).toDate(), $dayjs(date).toDate()) // The library specification requires two dates to be given
  const is_holiday = holidays.length
    ? `${holidays[0].name.split(/\s/)[1]}(${holidays[0].name.split(/\s/)[0]})`
    : ['土', '日'].includes(day)
    ? '休日'
    : ''
  const time = $dayjs().format('HH:mm:ss')
  const service = SERVICE[gid]
  const Sex = SEX_LABEL[sex]

  const dataset: Data[] = []
  for (const result of results ?? []) {
    const { name, age, residence } = result
    const gen = age2gen(age)
    const pref = await residence2pref(residence) // Parallel execution is not possible
    const area = residence2area(residence, pref)
    const data = { age, area, code, date, day, gen, gid, is_holiday, name, pref, service, sex: Sex, time }
    dataset.push(data)
  }

  // deduplication check::
  const records = (await database('user').where({ date, gid })) as Record[]

  const entries = compact(
    dataset.map(data => {
      const { name, age, pref } = data
      const index = records.findIndex(record => record.name === name && record.age === age && record.pref === pref)
      // If it doesn't exist in the records, entry it
      return index === -1 ? data : undefined
    })
  )

  if (isDebug) console.table(entries)

  // insert table::
  if (entries.length) await database('user').insert(entries).catch(notifyError)

  // update (insert) table::
  const summary = ((await database('summary').where({ gid }).whereLike('created_at', `%${date}%`)) as Summary[])[0]
  const now = $dayjs().format('YYYY-MM-DD HH:mm:ss')

  const entry = { code, count: entries.length, created_at: now, gid, service, sex: Sex, updated_at: now, updates: 0 }

  if (!summary) await database('summary').insert(entry).catch(notifyError)

  if (summary?.count) {
    const update = { count: summary.count + entries.length, updated_at: now, updates: summary.updates + 1 }
    await database('summary').update(update).where({ gid }).whereLike('created_at', `%${date}%`).catch(notifyError)
  }

  // history
  const count_time = prettyMilliseconds(Date.now() - count_start)
  const history = { code, count: entries.length, count_time, created_at: now, gid, service, sex: Sex }
  await database('history').insert(history).catch(notifyError)

  // spreadsheets
  {
    const summary = ((await database('summary').where({ gid }).orderBy('id', 'desc').limit(1)) as Summary[])[0]
    const row = Math.floor((summary.id - 1) / crawlers.length) + 2 + 1 // 2 = header rows
    const sheets_range = `集計結果!${CELL[gid]}${row}:${CELL[gid]}${row}`
    await clearRows(sheets, sheets_range)
    await addRows(sheets, sheets_range, [[summary.count]], 'OVERWRITE')

    await clearRows(sheets, `集計テーブル!A${summary.id + 1}`)
    await addRows(
      sheets,
      `集計テーブル!A${summary.id + 1}`,
      [Object.values(summary) as (string | number)[]],
      'OVERWRITE'
    )

    const user = (await database('user').where({ date, gid, time })) as Record[]
    await addRows(
      sheets,
      '新規登録者テーブル!A2',
      user.map(object => Object.values(object) as (string | number)[]),
      'INSERT_ROWS'
    )

    const history = (await database('history').where({ gid }).orderBy('id', 'desc').limit(1)) as History[]
    await addRows(
      sheets,
      '履歴テーブル!A2',
      history.map(object => Object.values(object) as (string | number)[]),
      'INSERT_ROWS'
    )
  }
}

let compact: typeof import('lodash-es/compact').default

void (async (infinity = true, headless = true) => {
  const lodash = (await import('lodash-es')).default
  compact = lodash.compact

  // Wait for dynamic import to live
  await new Promise(resolve => setTimeout(resolve, 1000))

  const { sheets } = await authentication()

  // create table::
  await createUserTable().catch(notifyError)
  await createSummaryTable().catch(notifyError)
  await createHistoryTable().catch(notifyError)

  // browser launch::
  const browser = await chromium.launch({ headless })

  // crawler crawl::
  while (infinity) {
    const summary = ((await database('summary').orderBy('id', 'desc').limit(1)) as Summary[])[0]
    const id = summary?.id ?? 1
    const row = Math.floor((id - 1) / crawlers.length) + 2 + 1 // 2 = header rows
    const sheets_range = `集計結果!A${row}:A${row}`

    const date = $dayjs().format('YYYY-MM-DD')
    await clearRows(sheets, sheets_range)
    await addRows(sheets, sheets_range, [[date]], 'OVERWRITE')
    for (const crawler of crawlers) await run(crawler, browser, sheets)
  }

  // browser close::
  await browser.close()

  // database destroy::
  await database.destroy()
})()
